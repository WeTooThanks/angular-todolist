import { Component, OnInit } from '@angular/core';
import { Todo } from '../../models/todo'
import { TodoService } from '../../services/todo.service'

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  // Placeholder data
  todos:Todo[];

  // Used primarily to import services
  constructor(private todoService:TodoService) { }

  // Runs immediately upon initialization
  ngOnInit() {
    this.todoService.getTodos().subscribe(todos => { this.todos = todos });
  }

  deleteTodo(todo:Todo){
    this.todos = this.todos.filter(t => t.id !== todo.id); // Deletes the todo from the UI, but not the server
    this.todoService.deleteTodo(todo).subscribe(); // Calls on the todoService to delete the todo from the server
  }

  addTodo(todo:Todo){
    this.todoService.addTodo(todo).subscribe(todo => {this.todos.push(todo)});
  }

}
