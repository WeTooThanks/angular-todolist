import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Todo } from 'src/app/models/todo';
import { TodoService } from '../../services/todo.service'

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  @Input() todo:Todo; // Allows this component to act as a child to the parent todos component
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter();

  constructor(private todoService:TodoService) { }

  ngOnInit() {
  }

  // Set Dynamic Classes
  setClasses(){
    let classes = {
      todo: true,
      'is-complete': this.todo.completed
    }

    return classes;
  }

  onToggle(todo){
    todo.completed = !todo.completed; // Toggle in UI
    this.todoService.toggleCompleted(todo).subscribe(todo => console.log()); // Toggle on server
  }

  // Triggered by the clicking of the delte button in the UI
  onDelete(todo){
    this.deleteTodo.emit(todo); // emits up so that the todos component can change the UI to reflect the deletion
    }
}
