import { Component } from '@angular/core';

// @Component is a typescript decorator. Includes meta-data for the component
@Component({
  selector: 'app-root', // The name that will be used to render this template
  templateUrl: './app.component.html', // Points to the HTML template used for this component
  styleUrls: ['./app.component.css'] // Points to the Style Sheet used for this component
})

// All properties and methods for the component should go here
export class AppComponent {
  name:string = 'John';

  // Runs at instantiation
  constructor(){
  }
}
